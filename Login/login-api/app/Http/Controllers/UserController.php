<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    private $status_code = 200;



    public function createUser(Request $request) {

        // validate inputs
        $validator          =       Validator::make($request->all(),
            [
                "name"        =>      "required",
                "email"             =>      "required|email",
                "password"         =>      "required",
                "services"             =>      "required",
                "role"             =>      "required"
            ]
        );

        // if validation fails
        if($validator->fails()) {
            return response()->json(["status" => "failed", "validation_errors" => $validator->errors()]);
        }

        $user_id             =       $request->id;
        $userArray           =       array(
            "name"            =>      $request->name,
            "email"             =>      $request->email,
            "password"             =>      md5($request->password),
            "services"                 =>      $request->services,
            "role"                 =>      $request->role,
        );

        if($user_id !="") {
            $user              =         User::find($user_id);
            if(!is_null($user)){
                $updated_status     =       User::where("id", $user_id)->update($userArray);
                if($updated_status == 1) {
                    return response()->json(["status" => $this->status_code, "success" => true, "message" => "user detail updated successfully"]);
                }
                else {
                    return response()->json(["status" => "failed", "message" => "Whoops! failed to update, try again."]);
                }
            }
        }

        else {
            $user        =       User::create($userArray);
            if(!is_null($user)) {
                return response()->json(["status" => $this->status_code, "success" => true, "message" => "user record created successfully", "data" => $user]);
            }
            else {
                return response()->json(["status" => "failed", "success" => false, "message" => "Whoops! failed to create."]);
            }
        }
    }

    function login(Request $request){


        $validator          =       Validator::make($request->all(),
            [
                "email"             =>          "required|email",
                "password"          =>          "required",
            ]
        );

        if($validator->fails()) {
            return response()->json(["status" => "failed", "validation_error" => $validator->errors()]);
        }


        // check if entered email exists in db
        $email_status       =       User::where("email", $request->email)->first();


        // if email exists then we will check password for the same email

        if(!is_null($email_status)) {
            $password_status    =   User::where("email", $request->email)->where("password", md5($request->password))->first();

            // if password is correct
            if(!is_null($password_status)) {
                $user           =       $this->userDetail($request->email);

                return response()->json(["status" => $this->status_code, "success" => true, "message" => "You have logged in successfully", "data" => $user]);
            }

            else {
                return response()->json(["status" => "failed", "success" => false, "message" => "Unable to login. Incorrect password."]);
            }
        }

        else {
            return response()->json(["status" => "failed", "success" => false, "message" => "Unable to login. Email doesn't exist."]);
        }
    }

    // ------------------ [ User Detail ] ---------------------
    public function userDetail($email) {
        $user               =       array();
        if($email != "") {
            $user           =       User::where("email", $email)->first();
            return $user;
        }
    }


    public function edit($id)
    {
        $user = User::find($id);
        return response()->json($user);
    }
}
