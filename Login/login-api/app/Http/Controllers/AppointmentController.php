<?php

namespace App\Http\Controllers;

use App\Models\Appointment;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AppointmentController extends Controller
{
    private $status_code = 200;



    public function createAppointment(Request $request) {

        // validate inputs
        $validator          =       Validator::make($request->all(),
            [
                "id_user"        =>      "required",
                "id_doctor"             =>      "required",
                "date"         =>      "required",
            ]
        );

        // if validation fails
        if($validator->fails()) {
            return response()->json(["status" => "failed", "validation_errors" => $validator->errors()]);
        }

        $appointment_id            =       $request->id;
        $appointmentArray           =       array(
            "id_user"            =>      $request->id_user,
            "id_doctor"             =>      $request->id_doctor,
            "date"             =>      $request->date,
        );

        if($appointment_id !="") {
            $appointment              =         Appointment::find($appointment_id);
            if(!is_null($appointment)){
                $updated_status     =       User::where("id", $appointment_id)->update($appointmentArray);
                if($updated_status == 1) {
                    return response()->json(["status" => $this->status_code, "success" => true, "message" => "appointment detail updated successfully"]);
                }
                else {
                    return response()->json(["status" => "failed", "message" => "Whoops! failed to update, try again."]);
                }
            }
        }

        else {
            $appointment        =       Appointment::create($appointmentArray);
            if(!is_null($appointment)) {
                return response()->json(["status" => $this->status_code, "success" => true, "message" => "appointment record created successfully", "data" => $appointment]);
            }
            else {
                return response()->json(["status" => "failed", "success" => false, "message" => "Whoops! failed to create."]);
            }
        }
    }


    public function appointmentDetails($email)
    {
        $results = DB::table('appointments')
            ->select('appointments.date', 'users.name')
            ->join('users', 'users.id', '=', 'appointments.id_user')
            ->join('doctors', 'doctors.id', '=', 'appointments.id_doctor')
            ->where('doctors.email', '=', $email)
            ->groupBy('appointments.date',  'users.name')
            ->get();

        return response()->json($results);
    }

    public function appointmentsByDay($email)
    {

        $currentMonth = date('m');
        $results = DB::table('appointments')
            ->select(DB::raw('SUBSTR(date,9, 2) as day'), DB::raw('count(*) as number_of_appointments'))
            ->join('doctors', 'doctors.id', '=', 'appointments.id_doctor')
            ->where('doctors.email', '=', $email)
            ->where(DB::raw('SUBSTR(date,6, 2)'), '=', $currentMonth)
            ->groupBy(DB::raw("day"))
            ->get();

        return response()->json($results);
    }

}
