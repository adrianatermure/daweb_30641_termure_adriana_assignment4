<?php


namespace App\Http\Controllers;

use App\Models\Doctor;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class DoctorController1
{
    private $status     =   200;

    public function doctorsListening() {
        $doctors       =       Doctor::all();
        if(count($doctors) > 0) {
            return response()->json(["status" => $this->status, "success" => true, "count" => count($doctors), "data" => $doctors]);
        }
        else {
            return response()->json(["status" => "failed", "success" => false, "message" => "Whoops! no record found"]);
        }
    }

}
