<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;


class UsersTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([

            'name'      => 'Modan Alexandra',
            'email'     => 'modan.alexandra@yahoo.com',
            'password'  =>  md5('1234'),
            'services'  => 'Implantologie',
            'role'  => 'pacient'

        ]);
    }
}
