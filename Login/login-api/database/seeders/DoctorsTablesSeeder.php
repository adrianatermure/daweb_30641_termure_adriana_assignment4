<?php

namespace Database\Seeders;
use App\Models\Doctor;
use Illuminate\Database\Seeder;

class DoctorsTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Doctor::create([

            'name'      => 'Alice Gavrilescu',
            'email'     => 'alice.gavrilescu@yahoo.com',
            'password'  =>  '1234',
            'specialization'  => 'Implantologie'

        ]);
    }
}
