<?php

use App\Http\Controllers\AppointmentController;
use App\Http\Controllers\DoctorController1;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', [UserController::class, 'login']);
Route::get('/edit/{id}', [UserController::class, 'edit']);
Route::post("create-user",[UserController::class, 'createUser']);
Route::get('/doctorsListening', [DoctorController1::class, 'doctorsListening']);
Route::post("createAppointment",[AppointmentController::class, 'createAppointment']);
Route::get("/appointmentDetails/{email}",[AppointmentController::class, 'appointmentDetails']);
Route::get("/appointmentsByDay/{email}",[AppointmentController::class, 'appointmentsByDay']);
