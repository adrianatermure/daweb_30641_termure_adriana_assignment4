Tema 1
Proiectați și implementați o aplicație web pentru prezentarea unui cabinet stomatologic DentaWEB.
Cerințe:
- Folosiți un meniu de navigare care sa conțină cel puțin: Acasă, Noutăți, Despre Noi, Medici (nume,
prenume, specializare, poza profil), Servicii&Tarife si Contact (adresa, telefon, email). 2p
- Implementați pagini web specifice pentru elementele meniului de navigare 2p
- Folosiți elemente de HTML5 pentru organizarea informațiilor in paginile web dezvoltate 2p
- Inserați in pagini imagini si clipuri media corespunzătoare. 1p
- Stilizați paginile web folosind CSS 2p
Observații:
- Limbaje/tehnologii pentru implementare: HTML 5 si CSS.
- Folosiți validatoare pentru a arata ca paginile web nu au erori.
- Instalați aplicația web pe infrastructura configurata la Lab 1.
- La evaluare se vor considera următoarele aspecte legate de aplicație: meniuri, utilizare butoane pentru
navigare, design pagina, tabele, paginare, font-uri adecvate, imagini, etc.

Tema 2
Îmbunătățiți aplicația DentaWEB de la Tema 1 folosind specificațiile de mai jos.
Cerințe:
1. Implementați funcționalitatea de Selectare Limba (RO si EN). Salvarea preferinței utilizatorilor pentru
limba in care se afișează informația se va face folosind HTTP Cookies sau HTML 5 Web Storage. 3p
2. Folosiți JS pentru a prelua dinamic informațiile corespunzătoare meniului Noutăți din fișiere XML. 3p
3. Implementați un formular de trimitere email in pagina de Contact. 3p
Observații:
- Folosiți limbajele HTML 5, CSS si JS. Se accepta utilizarea oricărui framework existent care pune la
dispoziție librarii avansate pentru aceste limbaje (Sugestii: Vue.js, Angular, React sau jQuery).
- Pentru cerința nr 3 folosind Python sau Ruby (alegeți cel puțin un limbaj) pentru implementarea
componentelor pe partea de Server. Se accepta utilizarea oricărui Framework existent care pune la
dispoziție librarii avansate pentru aceste limbaje (Sugestie: Ruby on Rails, Django, Flask, etc.).

Tema 3
Plecând de la aplicația DentaWEB dezvoltata la Tema 2, implementați următoarele noi funcționalități:
1. Înregistrare / logare utilizator (email & parola). Pentru păstrarea stării utilizatorului după logare se vor
folosi sesiuni. 3p
2. Pentru utilizatorii logați:
2.1. Pagina de profil utilizator care conține următoarele informații ce pot fi modificate: Nume,
Email, Parola, Servicii medicale vizate (selectare mai multe valori din lista), Încărcare
consimțământ GDPR (posibilitate de încărcare a unui fișier pdf). 3p
2.2. Pagina pentru realizarea unei programări la un anumit medic într-o anumita zi. 3p
Observații:
• Se vor folosi tehnologii/limbaje JS si PHP pentru a implementa noile funcționalități (Sugestie: folosiți
servicii REST). Se accepta utilizarea oricărui Framework existent care pune la dispoziție librarii avansate
pentru aceste tehnologii (Sugestie: Laravel).
• Persistenta datelor se face folosind baze de date (Sugestie: MySQL).

Tema 4
Plecând de la aplicația DentaWEB dezvoltata la Tema 3 considerați următoarele noi cerințe:
• Implementați si integrați in aplicație următoarele noi servicii
o Creați pagina specifica pe care o pot accesa medicii folosind un cont alocat acestora in care sa
poată vizualiza un calendar cu programările sale (Sugestie: integrați un serviciu web pentru
calendar). 2.5p
o Creați o a doua pagina pentru contul medicilor care sa ofere o statistica pentru numărul de
programări primite pe zi in ultima luna sub forma unui grafic (Sugestie: integrați un serviciu web
pentru desenare de grafice). 2.5p
• Definiți cazuri de testare pentru noile funcționalități. Folosiți unelte si tehnici de testare pentru a le
valida. 2p
• Implementați funcționalitățile necesare pentru a respecta principii de securitate pentru aplicații web.
Faceți o prezentare a punctelor forte ale aplicației din punct de vedere a securității la laborator. 2p
Observații:
• Noile funcționalități vor fi implementate folosind orice limbaj de programare web. Se accepta utilizarea
oricărui Framework web existent.
