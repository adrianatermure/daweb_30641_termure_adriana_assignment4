import React, {Suspense} from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from "./App";
import reportWebVitals from './reportWebVitals';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Medici from "./menu/Medici";
import DespreNoi from "./menu/DespreNoi";
import Noutati from "./menu/Noutati";
import Servicii from "./menu/Servicii";
import Contact from "./menu/Contact";
import Login from "./login/Login";
import User from "./login/User";
import Logout from "./login/Logout";
import Appointment  from "./login/Appointment";
import Calendar from "./calendar/Calendar1";
import StatisticsPage from "./login/StatisticsPage";
import './multi-language-translate/i18n';
import Test from "./calendar/test";

import DropDownLanguage from "./DropDownLanguage";

const Routing=()=>{

    return(
    <React.Fragment>
        <Router>
            <Switch>
                <Route exact path="/" component={App}/>
                <Route exact path="/medici" component={Medici}/>
                <Route exact path="/desprenoi" component={DespreNoi}/>
                <Route exact path="/noutati" component={Noutati}/>
                <Route exact path="/servicii" component={Servicii}/>
                <Route exact path="/contact" component={Contact}/>
                <Route exact path="/login" component={Login}/>
                <Route exact path="/user" component={User}/>
                <Route exact path="/logout" component={Logout}/>
                <Route exact path="/appointment" component={Appointment}/>
                <Route exact path="/calendar" component={Calendar}/>
                <Route exact path="/statisticsPage" component={StatisticsPage}/>
                <Route exact path="/test" component={Test}/>
            </Switch>
        </Router>
    </React.Fragment>
    )
}
ReactDOM.render(
    <Suspense fallback={(<div>Loading</div>)}>
  <React.StrictMode>
      <DropDownLanguage />
    <Routing />
  </React.StrictMode>,
    </Suspense>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
