import React, { Component} from "react";
import { Table, Button } from "reactstrap";
import axios from "axios";
import EditUser from './EditUser';
import '../style/User.css';
import {Nav, Navbar} from "react-bootstrap";
import dental from "../img/dental.png";
import {LinkContainer} from "react-router-bootstrap";

import {Redirect} from "react-router-dom";

export default class User extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: [],
            isLoading: false,
            status: "",
            newUserModal: false,
            editUserData: {
                id: "",
                name: "",
                email: "",
                password: "",
                services: "",
                role: "",
            },
            editUserModal: false,
            noDataFound: "",
            update: false
        };
    }

    onLogoutHandler = () => {
        localStorage.clear();
        this.setState({
            navigate: true,
        });
    };


    componentDidMount() {

        const user = JSON.parse(localStorage.getItem("userData"));
        this.findUser(user.id);
    }
    findUser(id) {
        axios.get("http://localhost:8000/api/edit/" + id).then(res => {
            const user = res.data;
            this.setState({user});
        });
    }

    toggleNewUserModal = () => {
        this.setState({
            newUserModal: !this.state.newUserModal,
        });
    };

    toggleEditUserModal = () => {
        this.setState({
            editUserModal: !this.state.editUserModal,
            update: true
        });

    };

    onChangeEditUserHanler = (e) => {
        let { editUserData } = this.state;
        editUserData[e.target.name] = e.target.value;
        this.setState({ editUserData });
    };

    editUser = (id, name, email, password, services, role) => {
        this.setState({
            editUserData: { id, name, email, password, services, role },
            editUserModal: !this.state.editUserModal,
        });
    };

    updateUser = () => {
        let {
            id,
            name,
            email,
            password,
            services,
            role
        } = this.state.editUserData;
        this.setState({
            isLoading: true,
        });
        axios
            .post("http://localhost:8000/api/create-user", {
                name,
                email,
                password,
                services,
                role,
                id,
            })
            .then((response) => {
                this.setState({
                    editUserModal: false,
                    editUserData: { name, email, password, services, role },
                    isLoading:false,
                });

                const user = JSON.parse(localStorage.getItem("userData"));
                this.findUser(user.id);
            })
            .catch((error) => {
                this.setState({isLoading:false})
                console.log(error.response);
            });
    };


    render() {


        const { navigate } = this.state;
        if (navigate) {
            return <Redirect to="/" push={true} />;
        }


        const { editUserData, user} = this.state;



        return (

            <div className="color" >


                <Navbar className="navbar">

                    <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav className="mr-auto">

                            <div className="image">
                                <Nav>
                                    <img src={dental} height="200"/>
                                    <div className="centered">0758.074.415 | Str. Motilor Nr.32, Cluj-Napoca
                                    </div>
                                </Nav>
                            </div>


                            <div className="ml-auto">
                                <Nav>

                                    <LinkContainer to="/">
                                        <Nav.Link id={"home"}>Home</Nav.Link>
                                    </LinkContainer>
                                    <LinkContainer to="/noutati">
                                        <Nav.Link id={"nav-item"} >News</Nav.Link>
                                    </LinkContainer>
                                    <LinkContainer to="/desprenoi">
                                        <Nav.Link id={"nav-item"}>About Us</Nav.Link>
                                    </LinkContainer>
                                    <LinkContainer to="/medici">
                                        <Nav.Link id={"nav-item"} >Doctors</Nav.Link>
                                    </LinkContainer>

                                </Nav>
                                <Nav>

                                    <LinkContainer to="/servicii">
                                        <Nav.Link id={"nav-item"}>Services&Rates</Nav.Link>
                                    </LinkContainer>
                                    <LinkContainer to="/contact">
                                        <Nav.Link id={"nav-item"}>Contact</Nav.Link>
                                    </LinkContainer>

                                    <LinkContainer to="/login">
                                        <Nav.Link id={"nav-item"}>Login</Nav.Link>
                                    </LinkContainer>

                                    <LinkContainer to="/appointment">
                                        <Nav.Link id={"nav-item"}>Appointment </Nav.Link>
                                    </LinkContainer>


                                    <div className="col-xl-3 col-sm-12 col-md-3">
                                        <Button
                                            className="btn btn-primary text-right"
                                            onClick={this.onLogoutHandler}
                                        >
                                            Logout
                                        </Button>
                                    </div>


                                </Nav>

                            </div>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>





                <EditUser
                    toggleEditUserModal={this.toggleEditUserModal}
                    editUserModal={this.state.editUserModal}
                    onChangeEditUserHanler={this.onChangeEditUserHanler}
                    editUser={this.editUser}
                    editUserData={editUserData}
                    updateUser={this.updateUser}
                />
                <Table>
                    <thead>
                    <tr className="head">
                        <th>Id</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Password</th>
                        <th>Services</th>
                        <th>Role</th>
                        <th>Actions</th>
                    </tr>


                    <tr key={user.id}>
                        <td>{user.id}</td>
                        <td>{user.name}</td>
                        <td>{user.email}</td>
                        <td>{user.password}</td>
                        <td>{user.services}</td>
                        <td>{user.role}</td>
                        <td>
                            <Button
                                color="success"
                                className="mr-3"
                                size="sm"
                                onClick={() =>
                                    this.editUser(
                                        user.id,
                                        user.name,
                                        user.email,
                                        user.password,
                                        user.services,
                                        user.role
                                    )
                                }
                            >
                                Edit
                            </Button>

                        </td>
                    </tr>
                    </thead>

                </Table>

                <br></br>
                <br></br>
                <br></br>




                <footer>
                    <p>Dental Art</p>
                    <p><a href="mailto:dentalart@yahoo.com">dentalart@yahoo.com</a></p>

                </footer>

            </div>
        );
    }
}