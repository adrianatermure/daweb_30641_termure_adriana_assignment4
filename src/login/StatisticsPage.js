import React from 'react';
import {Bar} from 'react-chartjs-2';
import axios from "axios";
import {Nav, Navbar} from "react-bootstrap";
import dental from "../img/dental.png";
import {LinkContainer} from "react-router-bootstrap";
import {Button} from "reactstrap";
import {Redirect} from "react-router-dom";


export default class StatisticsPage extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
           days: [],
            numberOfAppointments: []
        };
    }


    componentDidMount() {

        const user = JSON.parse(localStorage.getItem("userData"));
        this.getNumberOfAppountmentsByDay(user.email);
    }
    getNumberOfAppountmentsByDay(email) {

        let days = [];
        let numberOfAppointments = [];

        axios.get("http://localhost:8000/api/appointmentsByDay/" + email).then(res => {

            for(const dataObj of res.data) {
                days.push(parseInt(dataObj.day));
                numberOfAppointments.push(parseInt(dataObj.number_of_appointments));

            }
            this.setState({days, numberOfAppointments});
            console.log(numberOfAppointments)
        });
    }

    onLogoutHandler = () => {
        localStorage.clear();
        this.setState({
            navigate: true,
        });
    };



    render() {

        const { navigate } = this.state;
        if (navigate) {
            return <Redirect to="/" push={true} />;
        }


        const state = {
            labels: this.state.days,
            datasets: [
                {
                    label: 'Appointments',
                    backgroundColor: 'rgba(75,192,192,1)',
                    borderColor: 'rgba(0,0,0,1)',
                    borderWidth: 2,
                    data: this.state.numberOfAppointments
                }
            ]
        }

        return (


            <div>

                <Navbar className="navbar">

                    <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav className="mr-auto">

                            <div className="image">
                                <Nav>
                                    <img src={dental} height="200"/>
                                    <div className="centered">0758.074.415 | Str. Motilor Nr.32, Cluj-Napoca
                                    </div>
                                </Nav>
                            </div>


                            <div className="ml-auto">
                                <Nav>

                                    <LinkContainer to="/">
                                        <Nav.Link id={"home"}>Home</Nav.Link>
                                    </LinkContainer>
                                    <LinkContainer to="/noutati">
                                        <Nav.Link id={"nav-item"} >News</Nav.Link>
                                    </LinkContainer>
                                    <LinkContainer to="/desprenoi">
                                        <Nav.Link id={"nav-item"}>About Us</Nav.Link>
                                    </LinkContainer>
                                    <LinkContainer to="/medici">
                                        <Nav.Link id={"nav-item"} >Doctors</Nav.Link>
                                    </LinkContainer>

                                </Nav>
                                <Nav>

                                    <LinkContainer to="/servicii">
                                        <Nav.Link id={"nav-item"}>Services&Rates</Nav.Link>
                                    </LinkContainer>
                                    <LinkContainer to="/contact">
                                        <Nav.Link id={"nav-item"}>Contact</Nav.Link>
                                    </LinkContainer>

                                    <LinkContainer to="/login">
                                        <Nav.Link id={"nav-item"}>Login</Nav.Link>
                                    </LinkContainer>

                                    <LinkContainer to="/appointment">
                                        <Nav.Link id={"nav-item"}>Appointment </Nav.Link>
                                    </LinkContainer>


                                    <div className="col-xl-3 col-sm-12 col-md-3">
                                        <Button
                                            className="btn btn-primary text-right"
                                            onClick={this.onLogoutHandler}
                                        >
                                            Logout
                                        </Button>
                                    </div>


                                </Nav>

                            </div>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>



            <div>
                <Bar
                    data={state}
                    options={{
                        title:{
                            display:true,
                            text:'The number of appointments received per day',
                            fontSize:20
                        },
                        legend:{
                            display:true,
                            position:'right'
                        }
                    }}
                />
            </div>


                <footer>
                    <p>Dental Art</p>
                    <p><a href="mailto:dentalart@yahoo.com">dentalart@yahoo.com</a></p>

                </footer>

            </div>
        );
    }
}