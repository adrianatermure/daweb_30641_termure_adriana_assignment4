import React, { Component } from "react";
import { Table, Button } from "reactstrap";
import axios from "axios";

import '../style/User.css';
import {Nav, Navbar} from "react-bootstrap";
import dental from "../img/dental.png";
import {LinkContainer} from "react-router-bootstrap";
import beige from "../img/beige.png";
import ModalAppointment from './ModalAppointment';
import {Redirect} from "react-router-dom";



export default class Appointment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            doctors: [],
            appointments: [],
            newAppointmentData: {
                id_user: "",
                id_doctor: "",
                date: "",
            },
            isLoading: false,
            status: "",
            newAppointmentModal: false,
            noDataFound: "",
            update: false,
        };
    }

    onLogoutHandler = () => {
        localStorage.clear();
        this.setState({
            navigate: true,
        });
    };


    componentDidMount() {

        this.doctorsListening();
    }
    doctorsListening() {
        axios.get("http://localhost:8000/api/doctorsListening").then((response) => {
            if (response.status === 200) {
                this.setState({
                    doctors: response.data.data ? response.data.data : [],
                });
            }
            if (
                response.data.status === "failed" &&
                response.data.success === false
            ) {
                this.setState({
                    noDataFound: response.data.message,
                });
            }
        });
    }

    toggleNewAppointmentModal = () => {
        this.setState({
            newAppointmentModal: !this.state.newAppointmentModal,
        });
    };


    onChangeAddAppointmentHandler = (e) => {
        let { newAppointmentData } = this.state;
        newAppointmentData[e.target.name] = e.target.value;
        this.setState({ newAppointmentData });
    };

    editUser = (id_doctor) => {
        this.setState({
            newAppointmentData: { id_doctor },
            newAppointmentModal: !this.state.newAppointmentModal,
        });
    };


    addAppointment = () => {
        const user = JSON.parse(localStorage.getItem("userData"));
        let {
            id_doctor,
            date,
        } = this.state.newAppointmentData;
        let id_user = user.id
        console.log(date)
        axios
            .post(
                "http://localhost:8000/api/createAppointment", {
                    id_user,
                    id_doctor,
                    date,
                })
            .then((response) => {
                const { appointments } = this.state;
                const newAppointments = [...appointments];
                newAppointments.push(response.data);
                this.setState(
                    {
                        appointments: newAppointments,
                        newAppointmentModal: false,
                        newAppointmentData: {
                            id_user: "",
                            id_doctor: "",
                            date: "",
                        },
                    },
                );
            });
    };



    render() {

        const { navigate } = this.state;
        if (navigate) {
            return <Redirect to="/" push={true} />;
        }



        const { noDataFound, newAppointmentData,doctors} = this.state;
        let doctorsDetails = [];
        if (doctors.length) {
            doctorsDetails = doctors.map((doctor) => {
                return (
                    <tr key={doctor.id}>

                        <td>{doctor.name}</td>
                        <td>{doctor.email}</td>
                        <td>{doctor.specialization}</td>
                        <td>
                            <Button
                                color="success"oca
                                className="mr-3"
                                size="sm"
                                onClick={() =>
                                    this.editUser(
                                        doctor.id
                                    )
                                }
                            >
                                Get appointment
                            </Button>
                        </td>
                    </tr>
                );
            });
        }

        return (

            <div>
                <Navbar className="navbar">

                    <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav className="mr-auto">

                            <div className="image">
                                <Nav>
                                    <img src={dental} height="200"/>
                                    <div className="centered">0758.074.415 | Str. Motilor Nr.32, Cluj-Napoca
                                    </div>
                                </Nav>
                            </div>


                            <div className="ml-auto">
                                <Nav>

                                    <LinkContainer to="/">
                                        <Nav.Link id={"home"}>Home</Nav.Link>
                                    </LinkContainer>
                                    <LinkContainer to="/noutati">
                                        <Nav.Link id={"nav-item"} >News</Nav.Link>
                                    </LinkContainer>
                                    <LinkContainer to="/desprenoi">
                                        <Nav.Link id={"nav-item"}>About Us</Nav.Link>
                                    </LinkContainer>
                                    <LinkContainer to="/medici">
                                        <Nav.Link id={"nav-item"} >Doctors</Nav.Link>
                                    </LinkContainer>

                                </Nav>
                                <Nav>

                                    <LinkContainer to="/servicii">
                                        <Nav.Link id={"nav-item"}>Services&Rates</Nav.Link>
                                    </LinkContainer>
                                    <LinkContainer to="/contact">
                                        <Nav.Link id={"nav-item"}>Contact</Nav.Link>
                                    </LinkContainer>

                                    <LinkContainer to="/login">
                                        <Nav.Link id={"nav-item"}>Login</Nav.Link>
                                    </LinkContainer>

                                    <LinkContainer to="/appointment">
                                        <Nav.Link id={"nav-item"}>Appointment </Nav.Link>
                                    </LinkContainer>


                                    <div className="col-xl-3 col-sm-12 col-md-3">
                                        <Button
                                            className="btn btn-primary text-right"
                                            onClick={this.onLogoutHandler}
                                        >
                                            Logout
                                        </Button>
                                    </div>


                                </Nav>

                            </div>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>

                <nav className="navbar-medici">
                    <img src={beige} height="100" width="100%"/>
                    <p className="textOverImage2">
                        Make an appointment
                    </p>
                </nav>

                <div className="App container mt-4">



                    <ModalAppointment
                        toggleNewAppointmentModal={this.toggleNewAppointmentModal}
                        newAppointmentModal={this.state.newAppointmentModal}
                        onChangeAddAppointmentHandler={this.onChangeAddAppointmentHandler}
                        addAppointment={this.addAppointment}
                        newAppointmentData={newAppointmentData}
                    />

                    <Table>
                        <thead>
                        <tr>

                            <th>Name</th>
                            <th>Email</th>
                            <th>Specialization</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        {doctors.length === 0 ? (
                            <tbody>
                            <h3>{noDataFound}</h3>
                            </tbody>
                        ) : (
                            <tbody>{doctorsDetails}</tbody>

                        )}
                    </Table>
                </div>


                <footer>
                    <p>Dental Art</p>
                    <p><a href="mailto:dentalart@yahoo.com">dentalart@yahoo.com</a></p>

                </footer>
            </div>
        );
    }
}

















































































