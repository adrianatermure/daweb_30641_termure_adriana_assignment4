import React, { Component } from "react";
import '../style/EditUser.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import {
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    FormGroup,
    Label,
    Input,
} from "reactstrap";

export default class EditUser extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedValue: "",
            val:""
        };
    }

    getSelectValue = () => {

        this.setState({

            selectedValue:", "+document.getElementById("list").value
        })

        this.props.editUserData.services +=", " + document.getElementById("list").value

    }

    render() {

        return (
            <div>
                <Modal
                    isOpen={this.props.editUserModal}
                    toggle={this.props.toggleEditUserModal}
                >
                    <ModalHeader toggle={this.props.toggleEditUserModal}>
                        Update User
                    </ModalHeader>
                    <ModalBody>
                        <FormGroup>
                            <Label for="name">Name</Label>
                            <Input
                                id="name"
                                name="name"
                                value={this.props.editUserData.name}
                                onChange={this.props.onChangeEditUserHanler}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="email">Email</Label>
                            <Input
                                id="email"
                                name="email"
                                value={this.props.editUserData.email}
                                onChange={this.props.onChangeEditUserHanler}
                            />
                        </FormGroup>

                        <FormGroup>
                            <Label for="password">Password</Label>
                            <Input
                                type="password"
                                id="password"
                                name="password"
                                value={this.props.editUserData.password}
                                onChange={this.props.onChangeEditUserHanler}
                            />
                        </FormGroup>

                        <FormGroup>
                            <Label for="role">Role</Label>
                            <Input
                                id="role"
                                name="role"
                                value={this.props.editUserData.role}
                                onChange={this.props.onChangeEditUserHanler}
                            />

                        </FormGroup>
                        <FormGroup>
                            <Label for="services">Services</Label>
                            <Input
                                id="services"
                                name="services"
                                value={this.props.editUserData.services}
                                onChange={this.props.onChangeEditUserHanler}
                            />

                        </FormGroup>


                        <select id="list" onChange={this.getSelectValue}>
                            <option value="Terapia cariilor">Terapia cariilor</option>
                            <option value="Estetica dentara">Estetica dentara</option>
                            <option value="Implantologie">Implantologie</option>
                            <option value="Chirurgie orala">Chirurgie orala</option>
                        </select>

                    </ModalBody>
                    <ModalFooter>
                        <Button
                            color="primary"
                            onClick={this.props.updateUser}
                        >
                            Update

                        </Button>
                        <Button
                            color="secondary"
                            onClick={this.props.toggleEditUserModal}
                        >
                            Cancel
                        </Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}