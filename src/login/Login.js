import React, { Component } from "react";
import { Button, Form, FormGroup, Label, Input } from "reactstrap";
import axios from "axios";
import { Redirect } from "react-router-dom";
import '../style/Login.css';
import {Nav, Navbar} from "react-bootstrap";
import dental from "../img/dental.png";
import {LinkContainer} from "react-router-bootstrap";
import white from "../img/white.png";


export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: "",
            msg: "",
            isLoading: false,
            redirect: false,
            errMsgEmail: "",
            errMsgPwd: "",
            errMsg: "",
        };
    }

    onLogoutHandler = () => {
        localStorage.clear();
        this.setState({
            navigate: true,
        });
    };

    onChangehandler = (e) => {
        let name = e.target.name;
        let value = e.target.value;
        let data = {};
        data[name] = value;
        this.setState(data);
    };

    onSignInHandler = () => {
        this.setState({ isLoading: true });
        axios
            .post("http://localhost:8000/api/login", {
                email: this.state.email,
                password: this.state.password,
            })
            .then((response) => {
                this.setState({ isLoading: false });
                if (response.data.status === 200) {
                    localStorage.setItem("isLoggedIn", true);
                    localStorage.setItem("userData", JSON.stringify(response.data.data));
                    this.setState({
                        msg: response.data.message,
                        redirect: true,
                    });
                }
                if (
                    response.data.status === "failed" &&
                    response.data.success === undefined
                ) {
                    this.setState({
                        errMsgEmail: response.data.validation_error.email,
                        errMsgPwd: response.data.validation_error.password,
                    });
                    setTimeout(() => {
                        this.setState({ errMsgEmail: "", errMsgPwd: "" });
                    }, 2000);
                } else if (
                    response.data.status === "failed" &&
                    response.data.success === false
                ) {
                    this.setState({
                        errMsg: response.data.message,
                    });
                    setTimeout(() => {
                        this.setState({ errMsg: "" });
                    }, 2000);
                }
            })
            .catch((error) => {
                console.log(error);
            });
    };

    render() {

        const { navigate } = this.state;
        if (navigate) {
            return <Redirect to="/" push={true} />;
        }

        if (this.state.redirect) {

            const user = JSON.parse(localStorage.getItem("userData"));


            if(user.role ==="doctor"){

                return <Redirect to="/calendar" />;
            }
            else{
                return <Redirect to="/user" />;
            }
        }
        const login = localStorage.getItem("isLoggedIn");
        if (login) {
            return <Redirect to="/user" />;
        }
        const isLoading = this.state.isLoading;


        return (

            <div>

                <Navbar className="navbar">

                    <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav className="mr-auto">

                            <div className="image">
                                <Nav>
                                    <img src={dental} height="200"/>
                                    <div className="centered">0758.074.415 | Str. Motilor Nr.32, Cluj-Napoca
                                    </div>
                                </Nav>
                            </div>


                            <div className="ml-auto">
                                <Nav>

                                    <LinkContainer to="/">
                                        <Nav.Link id={"home"}>Home</Nav.Link>
                                    </LinkContainer>
                                    <LinkContainer to="/noutati">
                                        <Nav.Link id={"nav-item"} >News</Nav.Link>
                                    </LinkContainer>
                                    <LinkContainer to="/desprenoi">
                                        <Nav.Link id={"nav-item"}>About Us</Nav.Link>
                                    </LinkContainer>
                                    <LinkContainer to="/medici">
                                        <Nav.Link id={"nav-item"} >Doctors</Nav.Link>
                                    </LinkContainer>

                                </Nav>
                                <Nav>

                                    <LinkContainer to="/servicii">
                                        <Nav.Link id={"nav-item"}>Services&Rates</Nav.Link>
                                    </LinkContainer>
                                    <LinkContainer to="/contact">
                                        <Nav.Link id={"nav-item"}>Contact</Nav.Link>
                                    </LinkContainer>

                                    <LinkContainer to="/login">
                                        <Nav.Link id={"nav-item"}>Login</Nav.Link>
                                    </LinkContainer>

                                    <LinkContainer to="/appointment">
                                        <Nav.Link id={"nav-item"}>Appointment </Nav.Link>
                                    </LinkContainer>


                                    <div className="col-xl-3 col-sm-12 col-md-3">
                                        <Button
                                            className="btn btn-primary text-right"
                                            onClick={this.onLogoutHandler}
                                        >
                                            Logout
                                        </Button>
                                    </div>


                                </Nav>

                            </div>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>

                <nav className="navbar-medici">
                    <img src={white} height="100" width="100%"/>
                    <p className="textOverImage2">
                        Login
                    </p>
                </nav>


                <Form className="containers" className = "Login">
                    <FormGroup>
                        <Label for="email">Email</Label>
                        <Input
                            type="email"
                            name="email"
                            placeholder="Enter email"
                            value={this.state.email}
                            onChange={this.onChangehandler}
                        />
                        <span className="text-danger">{this.state.msg}</span>
                        <span className="text-danger">{this.state.errMsgEmail}</span>
                    </FormGroup>
                    <FormGroup>
                        <Label for="password">Password</Label>
                        <Input
                            type="password"
                            name="password"
                            placeholder="Enter password"
                            value={this.state.password}
                            onChange={this.onChangehandler}
                        />
                        <span className="text-danger">{this.state.errMsgPwd}</span>
                    </FormGroup>
                    <p className="text-danger">{this.state.errMsg}</p>
                    <Button
                        className="text-center mb-4"
                        color="success"
                        onClick={this.onSignInHandler}
                    >
                        Sign In
                        {isLoading ? (
                            <span
                            className="spinner-border spinner-border-sm ml-5"
                            role="status"
                            aria-hidden="true"
                            />
                             ) : (
                            <span/>
                        )}
                    </Button>
                </Form>
            </div>
        );
    }
}