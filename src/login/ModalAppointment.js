import React, { Component} from "react";
import '../style/EditUser.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import 'bootstrap/dist/css/bootstrap.min.css';

import {
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    FormGroup,
    Label,
    Input,
} from "reactstrap";

export default class ModalAppointment extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedValue: "",
            startDate: new Date(),
            val:""
        };
        this.handleChange = this.handleChange.bind(this);
        this.onFormSubmit = this.onFormSubmit.bind(this);
    }

    handleChange(date) {
        this.setState({
            startDate: date
        })
        this.props.newAppointmentData.date =date
        console.log(date)
    }

    onFormSubmit(e) {
        e.preventDefault();
    }


    render() {

        const user = JSON.parse(localStorage.getItem("userData"));

        return (
            <div>
                <Modal
                    isOpen={this.props.newAppointmentModal}
                    toggle={this.props.toggleNewAppointmentModal}
                >
                    <ModalHeader toggle={this.props.toggleNewAppointmentModal}>
                       Make an appointment
                    </ModalHeader>
                    <ModalBody>
                        <FormGroup>
                            <Label for="id_user">Id user</Label>
                            <Input
                                id="id_user"
                                name="id_user"
                                value={user.id}
                                onChange={this.props.onChangeAddAppointmentHandler}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="id_doctor">Id doctor</Label>
                            <Input
                                id="id_doctor"
                                name="id_doctor"
                                value={this.props.newAppointmentData.id_doctor}
                                onChange={this.props.onChangeAddAppointmentHandler}
                            />
                        </FormGroup>


                        <FormGroup>
                            <Label for="date">Date and hour</Label>
                            <br></br>
                            <DatePicker
                                id="date"
                                name="date"
                                selected={ this.state.startDate }
                                onChange={ this.handleChange }
                                showTimeSelect
                                timeFormat="HH:mm"
                                timeIntervals={60}
                                timeCaption="time"
                                dateFormat="MMMM d, yyyy h:mm aa"
                            />
                        </FormGroup>


                    </ModalBody>
                    <ModalFooter>
                        <Button
                            color="primary"
                            onClick={this.props.addAppointment}
                        >
                            Add

                        </Button>
                        <Button
                            color="secondary"
                            onClick={this.props.toggleNewAppointmentModal}
                        >
                            Cancel
                        </Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}