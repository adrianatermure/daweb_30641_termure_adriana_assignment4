import {Container, Nav, Navbar} from "react-bootstrap";
import dental from "../img/dental.png";
import {LinkContainer} from "react-router-bootstrap";
import '../style/Noutati.css';
import React from "react";
import noutate1 from "../img/noutate1.png";
import noutate2 from "../img/noutate2.png";
import noutate4 from "../img/noutate4.png";
import noutate6 from "../img/noutate6.png";
import noutate7 from "../img/noutate7.png";
import beige from "../img/beige.png";
import axios from "axios";


import { useTranslation } from 'react-i18next';
import xmlData from "../Noutati.xml";
import xmlData1 from "../Noutati1.xml";
import {Button} from "reactstrap";

function onLogoutHandler() {
    localStorage.clear();
    window.location.href = '/'
}


function Noutati(){

    const {t} = useTranslation();

    if(localStorage.getItem('language')==="ro"){

    axios.get(xmlData, {
        "Content-Type": "application/xml; charset=utf-8"
    })
        .then((response) => {

            let parser = new DOMParser();
            let xml = parser.parseFromString(response.data, "text/xml");

                document.getElementById("title1").innerHTML =
                    xml.getElementsByTagName("title")[0].childNodes[0].nodeValue;
                document.getElementById("content1").innerHTML =
                    xml.getElementsByTagName("content")[0].childNodes[0].nodeValue;
                document.getElementById("date1").innerHTML =
                    xml.getElementsByTagName("date")[0].childNodes[0].nodeValue;

                document.getElementById("title2").innerHTML =
                    xml.getElementsByTagName("title")[1].childNodes[0].nodeValue;
                document.getElementById("content2").innerHTML =
                    xml.getElementsByTagName("content")[1].childNodes[0].nodeValue;
                document.getElementById("date2").innerHTML =
                    xml.getElementsByTagName("date")[1].childNodes[0].nodeValue;

                document.getElementById("title3").innerHTML =
                    xml.getElementsByTagName("title")[2].childNodes[0].nodeValue;
                document.getElementById("content3").innerHTML =
                    xml.getElementsByTagName("content")[2].childNodes[0].nodeValue;
                document.getElementById("date3").innerHTML =
                    xml.getElementsByTagName("date")[2].childNodes[0].nodeValue;

                document.getElementById("title4").innerHTML =
                    xml.getElementsByTagName("title")[3].childNodes[0].nodeValue;
                document.getElementById("content4").innerHTML =
                    xml.getElementsByTagName("content")[3].childNodes[0].nodeValue;
                document.getElementById("date4").innerHTML =
                    xml.getElementsByTagName("date")[3].childNodes[0].nodeValue;

                document.getElementById("title5").innerHTML =
                    xml.getElementsByTagName("title")[4].childNodes[0].nodeValue;
                document.getElementById("content5").innerHTML =
                    xml.getElementsByTagName("content")[4].childNodes[0].nodeValue;
                document.getElementById("date5").innerHTML =
                    xml.getElementsByTagName("date")[4].childNodes[0].nodeValue;

        });
        }
    else
    {

        axios.get(xmlData1, {
            "Content-Type": "application/xml; charset=utf-8"
        })
            .then((response) => {

                let parser = new DOMParser();
                let xml = parser.parseFromString(response.data, "text/xml");


                document.getElementById("title1").innerHTML =
                    xml.getElementsByTagName("title")[0].childNodes[0].nodeValue;
                document.getElementById("content1").innerHTML =
                    xml.getElementsByTagName("content")[0].childNodes[0].nodeValue;
                document.getElementById("date1").innerHTML =
                    xml.getElementsByTagName("date")[0].childNodes[0].nodeValue;

                document.getElementById("title2").innerHTML =
                    xml.getElementsByTagName("title")[1].childNodes[0].nodeValue;
                document.getElementById("content2").innerHTML =
                    xml.getElementsByTagName("content")[1].childNodes[0].nodeValue;
                document.getElementById("date2").innerHTML =
                    xml.getElementsByTagName("date")[1].childNodes[0].nodeValue;

                document.getElementById("title3").innerHTML =
                    xml.getElementsByTagName("title")[2].childNodes[0].nodeValue;
                document.getElementById("content3").innerHTML =
                    xml.getElementsByTagName("content")[2].childNodes[0].nodeValue;
                document.getElementById("date3").innerHTML =
                    xml.getElementsByTagName("date")[2].childNodes[0].nodeValue;

                document.getElementById("title4").innerHTML =
                    xml.getElementsByTagName("title")[3].childNodes[0].nodeValue;
                document.getElementById("content4").innerHTML =
                    xml.getElementsByTagName("content")[3].childNodes[0].nodeValue;
                document.getElementById("date4").innerHTML =
                    xml.getElementsByTagName("date")[3].childNodes[0].nodeValue;

                document.getElementById("title5").innerHTML =
                    xml.getElementsByTagName("title")[4].childNodes[0].nodeValue;
                document.getElementById("content5").innerHTML =
                    xml.getElementsByTagName("content")[4].childNodes[0].nodeValue;
                document.getElementById("date5").innerHTML =
                    xml.getElementsByTagName("date")[4].childNodes[0].nodeValue;

            });

    }


    return (

        <body>
        <div className="Noutati">
            <Navbar className="navbar">
                <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="mr-auto">

                        <div className="image">
                            <Nav>
                                <img src={dental} height="200"/>
                                <div className="centered">0758.074.415 | Str. Motilor Nr.32, Cluj-Napoca
                                </div>
                            </Nav>
                        </div>
                        <div className="ml-auto">
                            <Nav>

                                <LinkContainer to="/">
                                    <Nav.Link id={"home"}>{t('Home.1')}</Nav.Link>
                                </LinkContainer>
                                <LinkContainer to="/noutati">
                                    <Nav.Link id={"nav-item"} >{t('Home.2')}</Nav.Link>
                                </LinkContainer>
                                <LinkContainer to="/desprenoi">
                                    <Nav.Link id={"nav-item"}>{t('Home.3')}</Nav.Link>
                                </LinkContainer>
                                <LinkContainer to="/medici">
                                    <Nav.Link id={"nav-item"} >{t('Home.4')}</Nav.Link>
                                </LinkContainer>

                            </Nav>
                            <Nav>

                                <LinkContainer to="/servicii">
                                    <Nav.Link id={"nav-item"}>{t('Home.5')}</Nav.Link>
                                </LinkContainer>
                                <LinkContainer to="/contact">
                                    <Nav.Link id={"nav-item"}>{t('Home.6')}</Nav.Link>
                                </LinkContainer>

                                <LinkContainer to="/login">
                                    <Nav.Link id={"nav-item"}>{t('Home.10')}</Nav.Link>
                                </LinkContainer>

                                <LinkContainer to="/appointment ">
                                    <Nav.Link id={"nav-item"}>Appointment </Nav.Link>
                                </LinkContainer>

                                <div className="col-xl-3 col-sm-12 col-md-3">
                                    <Button
                                        className="btn btn-primary text-right"
                                        onClick={onLogoutHandler}
                                    >
                                        Logout
                                    </Button>
                                </div>



                            </Nav>

                        </div>
                    </Nav>
                </Navbar.Collapse>

            </Navbar>

            <nav className="navbar-medici">
                <img src={beige} height="100" width="100%"/>
                <p className="textOverImage2">
                    {t('News.1')}
                </p>
            </nav>


            <br></br>

            <div id="projects" className="container">

                    <div className="row">
                        <div className="col">
                        </div>
                        <div className="col-md-4">

                           <img className="img-fluid img-prj"
                            src={noutate1}/>

                        </div>
                        <div className="col-md-7">
                            <h3 id = "title1"> </h3>
                            <p id = "content1"> </p>
                            <h6 id = "date1"> </h6>
                        </div>

                        <div className="col">
                        </div>
                    </div>


                <br></br>
                <br></br>
                <br></br>


                    <div className="row">
                        <div className="col">
                        </div>
                        <div className="col-md-4">
                            <img className="img-fluid img-prj"
                                 src={noutate2}/>

                        </div>
                        <div className="col-md-7">
                            <h3 id = "title2"> </h3>
                            <p id = "content2"> </p>
                            <h6 id = "date2"> </h6>
                        </div>
                        <div className="col">
                        </div>
                    </div>

                <br></br>
                <br></br>
                <br></br>


                <div className="row">
                        <div className="col">
                        </div>
                        <div className="col-md-4">
                            <img className="img-fluid img-prj"
                                 src={noutate4} />

                        </div>
                        <div className="col-md-7">
                            <h3 id = "title3"> </h3>
                            <p id = "content3"> </p>
                            <h6 id = "date3"> </h6>
                        </div>

                        <div className="col">
                        </div>
                    </div>


                <br></br>
                <br></br>
                <br></br>



                <div className="row">
                        <div className="col">
                        </div>
                        <div className="col-md-4">
                            <img className="img-fluid img-prj"
                                 src={noutate6}/>
                        </div>
                        <div className="col-md-7">
                            <h3 id = "title4"> </h3>
                            <p id = "content4"> </p>
                            <br></br>
                            <h6 id = "date4"> </h6>
                        </div>
                        <div className="col">
                        </div>
                    </div>

                <br></br>
                <br></br>
                <br></br>

                <div className="row">
                    <div className="col">
                    </div>
                    <div className="col-md-4">
                        <img className="img-fluid img-prj"
                             src={noutate7} />

                    </div>
                    <div className="col-md-7">
                        <h3 id = "title5"> </h3>
                        <p id = "content5"> </p>
                        <h6 id = "date5"> </h6>
                    </div>

                    <div className="col">
                    </div>
                </div>


                <br></br>
                <br></br>
                <br></br>


            </div>

            <footer>
                <p>Dental Art</p>


                <p><a
                    href="mailto:dentalart@yahoo.com">dentalart@yahoo.com</a></p>

            </footer>




        </div>
        </body>


    );

}

export default Noutati;

