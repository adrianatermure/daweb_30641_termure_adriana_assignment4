import {Container, Nav, Navbar} from "react-bootstrap";
import dental from "../img/dental.png";
import {LinkContainer} from "react-router-bootstrap";
import '../style/DespreNoi.css';
import receptie from "../img/receptie.png";
import zambete from "../img/zambete.png";
import React from "react";

import { useTranslation } from 'react-i18next';
import {Button} from "reactstrap";

function onLogoutHandler() {
    localStorage.clear();
    window.location.href = '/'
}


function DespreNoi(){

    const {t} = useTranslation();

    return (
        <div className="DespreNoi">
            <Navbar className="navbar">
                <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="mr-auto">

                        <div className="image">
                            <Nav>
                                <img src={dental} height="200"/>
                                <div className="centered">0758.074.415 | Str. Motilor Nr.32, Cluj-Napoca
                                </div>
                            </Nav>
                        </div>
                        <div className="ml-auto">
                            <Nav>

                                <LinkContainer to="/">
                                    <Nav.Link id={"home"}>{t('Home.1')}</Nav.Link>
                                </LinkContainer>
                                <LinkContainer to="/noutati">
                                    <Nav.Link id={"nav-item"} >{t('Home.2')}</Nav.Link>
                                </LinkContainer>
                                <LinkContainer to="/desprenoi">
                                    <Nav.Link id={"nav-item"}>{t('Home.3')}</Nav.Link>
                                </LinkContainer>
                                <LinkContainer to="/medici">
                                    <Nav.Link id={"nav-item"} >{t('Home.4')}</Nav.Link>
                                </LinkContainer>

                            </Nav>
                            <Nav>

                                <LinkContainer to="/servicii">
                                    <Nav.Link id={"nav-item"}>{t('Home.5')}</Nav.Link>
                                </LinkContainer>
                                <LinkContainer to="/contact">
                                    <Nav.Link id={"nav-item"}>{t('Home.6')}</Nav.Link>
                                </LinkContainer>

                                <LinkContainer to="/login">
                                    <Nav.Link id={"nav-item"}>{t('Home.10')}</Nav.Link>
                                </LinkContainer>

                                <LinkContainer to="/appointment ">
                                    <Nav.Link id={"nav-item"}>Appointment </Nav.Link>
                                </LinkContainer>

                                <div className="col-xl-3 col-sm-12 col-md-3">
                                    <Button
                                        className="btn btn-primary text-right"
                                        onClick={onLogoutHandler}
                                    >
                                        Logout
                                    </Button>
                                </div>


                            </Nav>

                        </div>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>


            <div className="container">

                <div className="about">
                    <div className="left">
                        <h1>{t('About.1')}</h1>
                            <p>{t('About.2')}</p>

                            <p>{t('About.3')}</p>

                            <p>{t('About.4')}</p>
                    </div>

                    <div className="right">
                        <img
                            src={receptie} height="600" width="500"/>
                    </div>
                </div>



                <div className="smile">

                    <div className="left">
                        <img
                            src={zambete} />
                    </div>
                    <div className="right">
                        <h1>{t('About.5')}</h1>
                            <p>{t('About.6')}
                            </p>

                            <p>{t('About.7')}</p>
                    </div>


                </div>


                <div className="video">
                    <div className="left">
                        <h1>{t('About.8')}</h1>
                        <p>{t('About.9')}</p>

                        <p>{t('About.10')}</p>

                    </div>

                    <div className="right">

                                <div className="embed-responsive embed-responsive-16by9">
                                    <iframe id="prezentare"width="280" height="157" src="https://www.youtube.com/embed/0S44U2-v_r8" frameBorder="0" allowFullScreen></iframe>

                                </div>
                    </div>
                </div>




            </div>

            <footer>
                <p>Dental Art</p>
                <p><a href="mailto:dentalart@yahoo.com">dentalart@yahoo.com</a></p>
            </footer>

        </div>
    );
}

export default DespreNoi;

