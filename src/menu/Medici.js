import React from 'react';
import { Navbar, NavItem, NavDropdown, MenuItem, Nav, Form, FormControl, Button, Figure, Container, Row, Col , Jumbotron, Card, Carousel,ListGroup, ListGroupItem} from 'react-bootstrap';
import '../style/Medici.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import dental from "../img/dental.png";
import {LinkContainer} from "react-router-bootstrap";
import beige from '../img/beige.png'
import doctor2 from '../img/doctor2.png'
import doctor3 from '../img/doctor3.png'
import doctor4 from '../img/doctor4.png'
import doctor5 from '../img/doctor5.png'
import doctor6 from '../img/doctor6.png'
import doctor7 from '../img/doctor7.png'

import { useTranslation } from 'react-i18next';

function onLogoutHandler() {
    localStorage.clear();
    window.location.href = '/'
}


function Medici(){

    const {t} = useTranslation();

    return (
        <div className="Medici">
            <Navbar className="navbar">
                <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="mr-auto">

                        <div className="image">
                            <Nav>
                                <img src={dental} height="200"/>
                                <div className="centered">0758.074.415 | Str. Motilor Nr.32, Cluj-Napoca
                                </div>
                            </Nav>
                        </div>
                        <div className="ml-auto">
                            <Nav>

                                <LinkContainer to="/">
                                    <Nav.Link id={"home"}>{t('Home.1')}</Nav.Link>
                                </LinkContainer>
                                <LinkContainer to="/noutati">
                                    <Nav.Link id={"nav-item"} >{t('Home.2')}</Nav.Link>
                                </LinkContainer>
                                <LinkContainer to="/desprenoi">
                                    <Nav.Link id={"nav-item"}>{t('Home.3')}</Nav.Link>
                                </LinkContainer>
                                <LinkContainer to="/medici">
                                    <Nav.Link id={"nav-item"} >{t('Home.4')}</Nav.Link>
                                </LinkContainer>

                            </Nav>
                            <Nav>

                                <LinkContainer to="/servicii">
                                    <Nav.Link id={"nav-item"}>{t('Home.5')}</Nav.Link>
                                </LinkContainer>
                                <LinkContainer to="/contact">
                                    <Nav.Link id={"nav-item"}>{t('Home.6')}</Nav.Link>
                                </LinkContainer>

                                <LinkContainer to="/login">
                                    <Nav.Link id={"nav-item"}>{t('Home.10')}</Nav.Link>
                                </LinkContainer>

                                <LinkContainer to="/appointment ">
                                    <Nav.Link id={"nav-item"}>Appointment </Nav.Link>
                                </LinkContainer>

                                <div className="col-xl-3 col-sm-12 col-md-3">
                                    <Button
                                        className="btn btn-primary text-right"
                                        onClick={onLogoutHandler}
                                    >
                                        Logout
                                    </Button>
                                </div>


                            </Nav>

                        </div>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>

            <nav className="navbar-medici">
                <img src={beige} height="100" width="100%"/>
                        <p className="textOverImage2">
                            {t('Medici.1')}
                        </p>
            </nav>


            <div className="container">
                <Container fluid>

            <div className="cards">


                <div className="card">

                    <img id="img-medici" src={doctor2}/>
                    <div className="body">
                        <h4>{t('Medici.2')}</h4>
                        <h5>{t('Medici.3')}</h5>
                        <p>{t('Medici.4')} </p>

                    </div>

                </div>

                <div className="card">

                    <img id="img-medici" src={doctor3}/>
                    <div className="body">
                        <h4>{t('Medici.5')}</h4>
                        <h5>{t('Medici.6')}</h5>
                        <p> {t('Medici.7')}</p>

                    </div>

                </div>

                <div className="card">

                    <img id="img-medici" src={doctor4} />
                    <div className="body">
                        <h4>{t('Medici.8')}</h4>
                        <h5>{t('Medici.9')}</h5>
                        <p>{t('Medici.10')}</p>

                    </div>

                </div>


            </div>


                    <div className="cards1 cards">


                        <div className="card">

                            <img id="img-medici" src={doctor5} />
                            <div className="body">
                                <h4>{t('Medici.11')}</h4>
                                <h5> {t('Medici.12')}</h5>
                                <p>{t('Medici.13')}</p>

                            </div>

                        </div>

                        <div className="card">

                            <img id="img-medici" src={doctor6}/>
                            <div className="body">
                                <h4>{t('Medici.14')}</h4>
                                <h5>{t('Medici.15')}</h5>
                                <p> {t('Medici.16')}</p>

                            </div>

                        </div>

                        <div className="card">

                            <img id="img-medici" src={doctor7} />
                            <div className="body">
                                <h4>{t('Medici.17')}</h4>
                                <h5>{t('Medici.18')}</h5>
                                <p>{t('Medici.19')}</p>

                            </div>

                        </div>


                    </div>


                </Container>

            </div>


            <footer id="footer-medici">
                <p>Dental Art</p>
                <p><a href="mailto:dentalart@yahoo.com">dentalart@yahoo.com</a></p>
            </footer>

        </div>
    );
}

export default Medici;

