import {Nav, Navbar,ListGroup} from "react-bootstrap";
import dental from "../img/dental.png";
import {LinkContainer} from "react-router-bootstrap";
import '../style/Contact.css';
import React from "react";
import beige from "../img/beige.png";
import { useTranslation } from 'react-i18next';
import axios from "axios";
import {Button} from "reactstrap";



function onLogoutHandler() {
    localStorage.clear();
    window.location.href = '/'
}


function sendEmail(){


    axios.post("http://127.0.0.1:8000/mail",{}, {params: {nume : document.getElementById("name").value,
            email : document.getElementById("email").value,
            mesaj : document.getElementById("mesaj").value
            }
        }
    )
        .then(response => response.status)
        .catch(err => console.warn(err));


}

function Contact(){

    const {t} = useTranslation();


        return (



        <div className="Contact">
            <Navbar className="navbar">

                <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="mr-auto">

                        <div className="image">
                            <Nav>
                                <img src={dental} height="200"/>
                                <div className="centered">0758.074.415 | Str. Motilor Nr.32, Cluj-Napoca
                                </div>
                            </Nav>
                        </div>


                        <div className="ml-auto">
                            <Nav>

                                <LinkContainer to="/">
                                    <Nav.Link id={"home"}>{t('Home.1')}</Nav.Link>
                                </LinkContainer>
                                <LinkContainer to="/noutati">
                                    <Nav.Link id={"nav-item"} >{t('Home.2')}</Nav.Link>
                                </LinkContainer>
                                <LinkContainer to="/desprenoi">
                                    <Nav.Link id={"nav-item"}>{t('Home.3')}</Nav.Link>
                                </LinkContainer>
                                <LinkContainer to="/medici">
                                    <Nav.Link id={"nav-item"} >{t('Home.4')}</Nav.Link>
                                </LinkContainer>

                            </Nav>
                            <Nav>

                                <LinkContainer to="/servicii">
                                    <Nav.Link id={"nav-item"}>{t('Home.5')}</Nav.Link>
                                </LinkContainer>
                                <LinkContainer to="/contact">
                                    <Nav.Link id={"nav-item"}>{t('Home.6')}</Nav.Link>
                                </LinkContainer>

                                <LinkContainer to="/login">
                                    <Nav.Link id={"nav-item"}>{t('Home.10')}</Nav.Link>
                                </LinkContainer>

                                <LinkContainer to="/appointment ">
                                    <Nav.Link id={"nav-item"}>Appointment </Nav.Link>
                                </LinkContainer>

                                <div className="col-xl-3 col-sm-12 col-md-3">
                                    <Button
                                        className="btn btn-primary text-right"
                                        onClick={onLogoutHandler}
                                    >
                                        Logout
                                    </Button>
                                </div>



                            </Nav>

                        </div>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>

            {/*Contact*/}

            <section id="contact">


                <nav className="navbar-medici">
                    <img src={beige} height="100" width="100%"/>
                    <p className="textOverImage2">
                        {t('Contact.1')}
                    </p>
                </nav>

                <div class="contact-wrapper">

                    {/*Contact --left */}

                    <form id="contact-form" class="form-horizontal" role="form">

                        <p id="conditii">
                            {t('Contact.2')} </p>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="name" placeholder= {t('Contact.3')} name="name"  required/>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-12">
                            <input type="email" class="form-control" id="email" placeholder="EMAIL" name="email"  required/>
                        </div>
                    </div>


                    <textarea class="form-control" id ="mesaj" rows="10" placeholder={t('Contact.7')} name="message" required></textarea>

                    <button class="btn btn-primary send-button" id="submit" type="submit" value="SEND" onClick={sendEmail}>
                        <div class="button">
                            <i class="fa fa-paper-plane"></i><span class="send-text">{t('Contact.8')}</span>
                        </div>

                    </button>

                </form>


                    {/*Contact --right */}


                <div class="direct-contact-container">

                <ul class="contact-list">
                    <li class="list-item"><i class="fa fa-map-marker fa-2x"><span class="contact-text place">Str. Motilor Nr.32, Cluj-Napoca</span></i></li>

                    <li class="list-item"><i class="fa fa-phone fa-2x"><span class="contact-text phone"><a href="tel:0758.074.415" title="Give me a call">0758.074.415</a></span></i></li>

                    <li class="list-item"><i class="fa fa-envelope fa-2x"><span class="contact-text gmail"><a href="mailto:#" title="Send me an email">dentalart@yahoo.com</a></span></i></li>

                </ul>


                            <ListGroup id="orar">
                                <ListGroup.Item id="program">Program</ListGroup.Item>
                                <ListGroup.Item id="zi">{t('Contact.9')}..........................................08am-03pm</ListGroup.Item>
                                <ListGroup.Item id="zi">{t('Contact.10')}.........................................08am-03pm</ListGroup.Item>
                                <ListGroup.Item id="zi">{t('Contact.11')}....................................08am-03pm</ListGroup.Item>
                                <ListGroup.Item id="zi">{t('Contact.12')}............................................08am-03pm</ListGroup.Item>
                                <ListGroup.Item id="zi">{t('Contact.13')}.......................................08am-03pm</ListGroup.Item>
                                <ListGroup.Item id="zi">{t('Contact.14')}..................................Inchis</ListGroup.Item>
                                <ListGroup.Item id="zi">{t('Contact.15')}..................................Inchis</ListGroup.Item>
                            </ListGroup>


        </div>

</div>

</section>


            <footer>
                <p>Dental Art</p>
                <p><a href="mailto:dentalart@yahoo.com">dentalart@yahoo.com</a></p>

            </footer>

</div>
    );
}

export default Contact;

