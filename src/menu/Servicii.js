import {Container, Nav, Navbar, Carousel, Col, Row,} from "react-bootstrap";
import dental from "../img/dental.png";
import {LinkContainer} from "react-router-bootstrap";
import '../style/Servicii.css';
import React from "react";
import ss1 from "../img/ss1.png";
import ss2 from "../img/ss2.png";
import ss3 from "../img/ss3.png";
import ss4 from "../img/ss4.png";
import carousel1 from "../img/carousel1.png";
import carousel2 from "../img/carousel2.png";
import carousel3 from "../img/carousel3.png";
import beige from "../img/beige.png";

import { useTranslation } from 'react-i18next';
import {Button} from "reactstrap";


function onLogoutHandler() {
    localStorage.clear();
    window.location.href = '/'
}


function Servicii(){

    const {t} = useTranslation();

    return (
        <div className="Servicii">
            <Navbar className="navbar">
                <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="mr-auto">

                        <div className="image">
                            <Nav>
                                <img src={dental} height="200"/>
                                <div className="centered">0758.074.415 | Str. Motilor Nr.32, Cluj-Napoca
                                </div>
                            </Nav>
                        </div>
                        <div className="ml-auto">
                            <Nav>

                                <LinkContainer to="/">
                                    <Nav.Link id={"home"}>{t('Home.1')}</Nav.Link>
                                </LinkContainer>
                                <LinkContainer to="/noutati">
                                    <Nav.Link id={"nav-item"} >{t('Home.2')}</Nav.Link>
                                </LinkContainer>
                                <LinkContainer to="/desprenoi">
                                    <Nav.Link id={"nav-item"}>{t('Home.3')}</Nav.Link>
                                </LinkContainer>
                                <LinkContainer to="/medici">
                                    <Nav.Link id={"nav-item"} >{t('Home.4')}</Nav.Link>
                                </LinkContainer>

                            </Nav>
                            <Nav>

                                <LinkContainer to="/servicii">
                                    <Nav.Link id={"nav-item"}>{t('Home.5')}</Nav.Link>
                                </LinkContainer>
                                <LinkContainer to="/contact">
                                    <Nav.Link id={"nav-item"}>{t('Home.6')}</Nav.Link>
                                </LinkContainer>
                                <LinkContainer to="/login">
                                    <Nav.Link id={"nav-item"}>{t('Home.10')}</Nav.Link>
                                </LinkContainer>

                                <LinkContainer to="/appointment ">
                                    <Nav.Link id={"nav-item"}>Appointment </Nav.Link>
                                </LinkContainer>

                                <div className="col-xl-3 col-sm-12 col-md-3">
                                    <Button
                                        className="btn btn-primary text-right"
                                        onClick={onLogoutHandler}
                                    >
                                        Logout
                                    </Button>
                                </div>

                            </Nav>
                        </div>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>

            <nav className="navbar-medici">
                <img src={beige} height="100" width="100%"/>
                <p className="textOverImage2">
                    {t('Services.1')}
                </p>
            </nav>
            <section id="features">

                <div className="container-fluid features-box">
                    <div className="row">
                        <div className="feature-box col-lg-4 col-md-4 col-sm-12 ">
                            <img src={ss1}/>
                            <h4> {t('Services.2')}</h4>
                            <p>{t('Services.3')}</p>
                        </div>
                        <div className="feature-box col-lg-4 col-md-4 col-sm-12">
                            <img src={ss2}/>
                            <h4>{t('Services.4')}</h4>
                            <p>{t('Services.5')}</p>
                        </div>
                        <div className="feature-box col-lg-4 col-md-4 col-sm-12">
                            <img src={ss3}/>
                            <h4>{t('Services.6')}</h4>
                            <p>{t('Services.7')}</p>
                        </div>


                        <div className="feature-box col-lg-4 col-md-4 col-sm-12">
                            <br></br>
                            <br></br>
                            <br></br>
                            <img src={ss4}/>
                            <h4>{t('Services.8')}</h4>
                            <p>{t('Services.9')}</p>

                        </div>


                    </div>
                </div>
            </section>


            <div className="container container-servicii">
                <br></br>
                <br></br>
                <br></br>

                <Container fluid>
                    <Row>
                        <Col md={9} className="carousel-content carousel-servicii">
                            <Carousel fade>
                                <Carousel.Item>
                                    <img
                                        className="d-block w-100"
                                        src={carousel1}
                                        alt="React logo"
                                        height="400"
                                    />
                                    <Carousel.Caption>
                                        <h3>{t('Services.10')}</h3>
                                    </Carousel.Caption>
                                </Carousel.Item>
                                <Carousel.Item>
                                    <img
                                        className="d-block w-100"
                                        src={carousel2}
                                        alt="React logo"
                                        height="400"
                                    />

                                    <Carousel.Caption>
                                        <h3>{t('Services.11')}</h3>

                                    </Carousel.Caption>
                                </Carousel.Item>
                                <Carousel.Item>
                                    <img
                                        className="d-block w-100"
                                        src={carousel3}
                                        alt="React logo"
                                        height="400"
                                    />

                                    <Carousel.Caption>
                                        <h3>{t('Services.12')}</h3>

                                    </Carousel.Caption>
                                </Carousel.Item>
                            </Carousel>
                        </Col>
                    </Row>

                </Container>
            </div>



            <br></br>
            <br></br>


            <br></br>
            <br></br>


            <table>
                <thead>
                <tr>
                    <th id="tarife">{t('Services.13')}</th>
                    <th id="tarife"></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{t('Services.14')}</td>
                    <td>{t('Services.15')}</td>
                </tr>
                <tr>
                    <td id="rand-colorat">{t('Services.16')}</td>
                    <td id="rand-colorat">{t('Services.17')}</td>
                </tr>
                <tr>
                    <td>{t('Services.18')}</td>
                    <td>{t('Services.19')}</td>
                </tr>
                <tr>
                    <td id="rand-colorat">{t('Services.20')}</td>
                    <td id="rand-colorat">{t('Services.21')}</td>
                </tr>
                <tr>
                    <td>{t('Services.22')}</td>
                    <td>{t('Services.23')}</td>
                </tr>
                <tr>
                    <td id="rand-colorat">{t('Services.24')}</td>
                    <td id="rand-colorat">{t('Services.25')}</td>
                </tr>
                <tr>
                    <td>{t('Services.26')}</td>
                    <td>{t('Services.27')}</td>
                </tr>
                <tr>
                    <td id="rand-colorat">{t('Services.28')}</td>
                    <td id="rand-colorat">{t('Services.29')}</td>
                </tr>
                <tr>
                    <td>{t('Services.30')}</td>
                    <td>{t('Services.31')}</td>
                </tr>
                <tr>
                    <td id="rand-colorat">{t('Services.32')}</td>
                    <td id="rand-colorat">{t('Services.33')}</td>
                </tr>
                <tr>
                    <td>{t('Services.34')}</td>
                    <td>{t('Services.35')}</td>
                </tr>
                <tr>
                    <td id="rand-colorat">{t('Services.36')}</td>
                    <td id="rand-colorat">{t('Services.37')}</td>
                </tr>
                <tr>
                    <td>{t('Services.38')}</td>
                    <td>{t('Services.39')}</td>
                </tr>
                <tr>
                    <td id="rand-colorat">{t('Services.40')}</td>
                    <td id="rand-colorat">{t('Services.41')}</td>
                </tr>
                <tr>
                    <td>{t('Services.42')}</td>
                    <td>{t('Services.43')}</td>
                </tr>
                <tr>
                    <td id="rand-colorat">{t('Services.44')}</td>
                    <td id="rand-colorat">{t('Services.45')}</td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                </tbody>
            </table>

            <footer>
                <p>Dental Art</p>
                <p><a href="mailto:dentalart@yahoo.com">dentalart@yahoo.com</a></p>
            </footer>


        </div>
    );
}

export default Servicii;

