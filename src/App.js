
import React, {Component} from 'react';
import { Navbar, NavItem, NavDropdown, MenuItem, Nav, Form, FormControl, Button, Figure, Container, Row, Col , Jumbotron, Card, Carousel,ListGroup, ListGroupItem} from 'react-bootstrap';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import dental from './img/dental.png';
import smile from './img/smile.jpg';
import dantura from './img/dantura.jpg';
import cabinet from './img/cabinet.png';
import white from './img/white.png';
import { LinkContainer } from 'react-router-bootstrap';

import {useTranslation } from 'react-i18next';

function onLogoutHandler() {
    localStorage.clear();
}

function App() {


    const {t} = useTranslation();

        return (

            <body>
            <div className="App">

                <Navbar class="navbar">
                    <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
                    <Navbar.Collapse id="responsive-navbar-nav">

                        <Nav className="mr-auto">

                            <div className="image">
                                <Nav>
                                    <img src={dental} height="200"/>
                                    <div className="centered">0758.074.415 | Str. Motilor Nr.32, Cluj-Napoca
                                    </div>

                                </Nav>

                            </div>

                            <div className="ml-auto">

                                <Nav>

                                    <LinkContainer to="/">
                                        <Nav.Link id={"home"}>{t('Home.1')}</Nav.Link>
                                    </LinkContainer>
                                    <LinkContainer to="/noutati">
                                        <Nav.Link id={"nav-item"}>{t('Home.2')}</Nav.Link>
                                    </LinkContainer>
                                    <LinkContainer to="/desprenoi">
                                        <Nav.Link id={"nav-item"}>{t('Home.3')}</Nav.Link>
                                    </LinkContainer>
                                    <LinkContainer to="/medici">
                                        <Nav.Link id={"nav-item"}>{t('Home.4')}</Nav.Link>
                                    </LinkContainer>

                                    <LinkContainer to="/statisticsPage">
                                        <Nav.Link id={"nav-item"}>Statistics</Nav.Link>
                                    </LinkContainer>

                                </Nav>
                                <Nav>

                                    <LinkContainer to="/servicii">
                                        <Nav.Link id={"nav-item"}>{t('Home.5')}</Nav.Link>
                                    </LinkContainer>
                                    <LinkContainer to="/contact">
                                        <Nav.Link id={"nav-item"}>{t('Home.6')}</Nav.Link>
                                    </LinkContainer>

                                    <LinkContainer to="/login">
                                        <Nav.Link id={"nav-item"}>{t('Home.10')}</Nav.Link>
                                    </LinkContainer>

                                    <LinkContainer to="/appointment">
                                        <Nav.Link id={"nav-item"}>Appointment </Nav.Link>
                                    </LinkContainer>


                                    <div className="col-xl-3 col-sm-12 col-md-3">
                                        <Button
                                            className="btn btn-primary text-right"
                                            onClick={onLogoutHandler}
                                        >
                                            Logout
                                        </Button>
                                    </div>


                                </Nav>

                            </div>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>

                <div className="container">
                    <Container fluid>
                        <Row>
                            <Col md={6} className="carousel-content">
                                <div className="animation">
                                    <img className="imageAnimation" src={dantura} width="700"
                                         height="550"/>
                                    <div className="centered" className="textAnimation">
                                        {t('Home.7')}
                                    </div>
                                </div>
                            </Col>
                            <Col md={3} className="carousel-content">
                                <div className="animation">
                                    <img className="imageAnimation" src={smile} width="730"
                                         height="550"/>

                                </div>
                            </Col>
                        </Row>

                        <Row className="card-row">
                            <p className="card-row1" id="treatment"> {t('Home.8')}</p>
                        </Row>
                        <Row>
                            <Col md={6} className="carousel-content">
                                <div className="animation">
                                    <img className="imageAnimation" src={cabinet} width="700"
                                         height="400"/>
                                </div>
                            </Col>
                            <Col md={3}>
                                <div>
                                    <img className="imageAnimation" src={white} width="600"
                                         height="400"/>
                                    <p className="textOverImage">
                                        Dental Art
                                    </p>

                                    <p className="textOverImage1">
                                        <br></br>
                                        &nbsp; &nbsp; &nbsp;
                                        {t('Home.9')}
                                    </p>
                                </div>
                            </Col>
                        </Row>

                        <Row className="card-row">
                            <p className="card-row1" id="treatment"></p>
                        </Row>
                    </Container>
                </div>


                <footer>
                    <p>Dental Art</p>
                    <p><a href="mailto:dentalart@yahoo.com">dentalart@yahoo.com</a></p>
                </footer>

            </div>

            </body>
        );

    }





export default App;

