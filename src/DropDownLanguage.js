import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import './DropDownLanguage.css';

const DropdownLanguage = () => {
    const { i18n } = useTranslation();


    const handleLangChange = evt => {
        localStorage.setItem('language',evt.target.value);
        const lang = evt.target.value;
        i18n.changeLanguage(lang);
    };

    const lng = localStorage.getItem('language');

    return (
        <div className ="drop-down">

            <select onChange={handleLangChange} value={lng}>
                <option value="ro">Romanian</option>
                <option value="en">English</option>
            </select>

        </div>

    );
};

export default DropdownLanguage;