import React, { Component } from 'react';

import { Calendar, momentLocalizer  } from 'react-big-calendar'
import moment from 'moment';
import 'react-big-calendar/lib/css/react-big-calendar.css';

import axios from 'axios'
import "react-big-calendar/lib/css/react-big-calendar.css";
import {Redirect} from "react-router-dom";

import {Nav, Navbar} from "react-bootstrap";
import dental from "../img/dental.png";
import {LinkContainer} from "react-router-bootstrap";
import {Button} from "reactstrap";


const localizer = momentLocalizer(moment)

export default class Calendar1 extends Component {

    constructor(props) {
        super(props);
        this.state = {
            events: [],

        };
    }

    componentDidMount() {

        const user = JSON.parse(localStorage.getItem("userData"));
        this.getEvents(user.email);
    }

    getEvents(email) {
        let events = [];
        axios.get("http://localhost:8000/api/appointmentDetails/" + email)
            .then(res => {

                for(const dataObj of res.data) {

                    const datetime = moment(dataObj.date);
                    console.log(moment(datetime.toDate()).format("HH:mm"));

                        events.push({
                        title: dataObj.name + " "+ moment(datetime.toDate()).format("HH:mm"),
                        start: new Date(dataObj.date),
                        end: new Date(dataObj.date),
                    })

                }
                this.setState({events});
                console.log(events)

            });
    }


    onLogoutHandler = () => {
        localStorage.clear();
        this.setState({
            navigate: true,
        });
    };


    render() {

        const { navigate } = this.state;
        if (navigate) {
            return <Redirect to="/" push={true} />;
        }

        return (



            <div>
                <Navbar className="navbar">

                    <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav className="mr-auto">

                            <div className="image">
                                <Nav>
                                    <img src={dental} height="200"/>
                                    <div className="centered">0758.074.415 | Str. Motilor Nr.32, Cluj-Napoca
                                    </div>
                                </Nav>
                            </div>


                            <div className="ml-auto">
                                <Nav>

                                    <LinkContainer to="/">
                                        <Nav.Link id={"home"}>Home</Nav.Link>
                                    </LinkContainer>
                                    <LinkContainer to="/noutati">
                                        <Nav.Link id={"nav-item"} >News</Nav.Link>
                                    </LinkContainer>
                                    <LinkContainer to="/desprenoi">
                                        <Nav.Link id={"nav-item"}>About Us</Nav.Link>
                                    </LinkContainer>
                                    <LinkContainer to="/medici">
                                        <Nav.Link id={"nav-item"} >Doctors</Nav.Link>
                                    </LinkContainer>

                                    <LinkContainer to="/statisticsPage">
                                        <Nav.Link id={"nav-item"}>Statistics</Nav.Link>
                                    </LinkContainer>



                                </Nav>
                                <Nav>

                                    <LinkContainer to="/servicii">
                                        <Nav.Link id={"nav-item"}>Services&Rates</Nav.Link>
                                    </LinkContainer>
                                    <LinkContainer to="/contact">
                                        <Nav.Link id={"nav-item"}>Contact</Nav.Link>
                                    </LinkContainer>

                                    <LinkContainer to="/login">
                                        <Nav.Link id={"nav-item"}>Login</Nav.Link>
                                    </LinkContainer>

                                    <LinkContainer to="/appointment">
                                        <Nav.Link id={"nav-item"}>Appointment </Nav.Link>
                                    </LinkContainer>


                                    <div className="col-xl-3 col-sm-12 col-md-3">
                                        <Button
                                            className="btn btn-primary text-right"
                                            onClick={this.onLogoutHandler}
                                        >
                                            Logout
                                        </Button>
                                    </div>


                                </Nav>

                            </div>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>




                <div className="App">
                    <header className="App-header">
                    </header>
                    <div style={{ height: 700 }}>
                        <Calendar
                            localizer={localizer}
                            events={this.state.events}
                            step={30}
                            defaultView='month'
                            views={['month','week','day']}
                            defaultDate={new Date()}
                        />
                    </div>
                </div>
            </div>
        );
    }
}
