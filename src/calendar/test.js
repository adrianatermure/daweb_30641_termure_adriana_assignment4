import React, {Component} from "react";
import axios from "axios";
import { Calendar, momentLocalizer } from "react-big-calendar";
import moment from "moment";
import "react-big-calendar/lib/css/react-big-calendar.css";
const localizer = momentLocalizer(moment);
const myEventsList = []

export default class DoctorAppointments extends Component{

    constructor(props) {
        super(props)
        this.state = {
            cal_events: [],
        }
    }

    componentDidMount() {
        axios.get('http://localhost:3001/events')
            .then(response => {

                let appointments = response.data;

                for (let i = 0; i < appointments.length; i++) {
                    appointments[i].start = moment.utc(appointments[i].start).toDate();
                    appointments[i].end = moment.utc(appointments[i].end).toDate();

                }
                this.setState({
                    cal_events:appointments
                })

            })
            .catch(function (error) {
                console.log(error);
            });
    }

    render(){
        return(
            <div>
                <Calendar
                    localizer={localizer}
                    events={myEventsList}
                    step = {30}
                    startAccessor="start"
                    endAccessor="end"
                />
            </div>

        );
    }
}